from django.db import models
from django.conf import settings

# Create your models here.
USER_AUTH = settings.AUTH_USER_MODEL


class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    members = models.ManyToManyField(USER_AUTH, related_name="projects")

    def __str__(self):
        return self.name
