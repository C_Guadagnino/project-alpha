from django.urls import path
from projects.views import (
    ProjectListView,
    ProjectDetailView,
    ProjectCreateView,
)
from tasks.views import TaskListView

urlpatterns = [
    path("", ProjectListView.as_view(), name="list_projects"),
    path("<int:pk>/", ProjectDetailView.as_view(), name="show_project"),
    path("tasks/", TaskListView.as_view(), name="list_tasks"),
    path("create/", ProjectCreateView.as_view(), name="create_project"),
]
