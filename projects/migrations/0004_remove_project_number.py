# Generated by Django 4.0.3 on 2022-03-30 00:17

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("projects", "0003_project_number"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="project",
            name="number",
        ),
    ]
